FROM alpine:3.7

LABEL maintainer lleandro21@gmail.com

RUN apk update \
    && apk add --no-cache varnish \
    && apk add git \
    && git clone https://github.com/varnish/varnish-modules.git \
    && apk add automake && apk add varnish-dev \
    && apk add autoconf && apk add libtool \
    && apk add py-docutils && apk add make \
    && cd varnish-modules/ \
    && ./bootstrap && ./configure && make && make install

COPY start.sh /usr/local/bin/docker-app-start

RUN chmod +x /usr/local/bin/docker-app-start

CMD ["docker-app-start"]

